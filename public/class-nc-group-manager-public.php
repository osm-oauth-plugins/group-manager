<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://neoweb.co.uk
 * @since      1.0.0
 *
 * @package    Nc_Group_Manager
 * @subpackage Nc_Group_Manager/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Nc_Group_Manager
 * @subpackage Nc_Group_Manager/public
 * @author     Jaco Mare <jaco.mare@neoweb.co.uk>
 */
class Nc_Group_Manager_Public {

    /**
     * @var array
     */
    private $plugin_data;

    /**
     * @param $key
     *
     * @return string
     */
    public function get_plugin_data($key): string {
        return $this->plugin_data[$key];
    }

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     */
	public function __construct() {
        $this->plugin_data = get_option('neoweb-connector-group-manager');
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Nc_Group_Manager_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Nc_Group_Manager_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->get_plugin_data('pluginName'), plugin_dir_url( __FILE__ ) . 'css/nc-group-manager-public.css', array(), $this->get_plugin_data('pluginVersion'), 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Nc_Group_Manager_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Nc_Group_Manager_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->get_plugin_data('pluginName'), plugin_dir_url( __FILE__ ) . 'js/nc-group-manager-public.js', array( 'jquery' ), $this->get_plugin_data('pluginVersion'), false );

	}

}
