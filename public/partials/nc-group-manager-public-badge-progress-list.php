<div class="card">
    <div class="nw_accordion">
        <h5 class="mb-0" data-toggle="collapse" data-target="#collapse<?php echo $scoutID; ?>" aria-expanded="true" aria-controls="collapse<?php echo $scoutID; ?>">
            <?php echo $name; ?>
            <?php if ($show_rank_badge == 1) : ?>
                <span class="badge badge-rank"><?php echo $lodge_level; ?></span>
            <?php endif; ?>
            <?php if ($show_lodge_badge == 1) : ?>
                <span class="badge badge-group pull-right"><?php echo $lodge; ?></span>
            <?php endif; ?>
        </h5>
    </div>
    <div class="nw_panel panel">
        <div class="card-body">
            <?php (new Nc_Group_Manager_Register_Plugin_Short_Codes)->fetch_badges_by_person ($scoutID, $badgeRecords, $badgetype, $showProgress, $showAwarded); ?>
        </div>
    </div>
</div>

