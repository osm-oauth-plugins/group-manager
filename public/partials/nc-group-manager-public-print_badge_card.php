<div class="col-md-3">
	<div class="card">
		<div class="card-body text-center">

            <img src="<?php echo $domain . '/' . $pictureUrl; ?>" alt="<?php echo $badgeLabel; ?> badge <?php //echo $level; ?>" title="<?php echo $badgeLabel; ?> badge <?php //echo $level; ?>" width="100" />

            <span class="card-text badgeLabel" style="font-size: 75%;">
				<?php echo $badgeLabel; ?>
			</span>
		</div>
	</div>
</div>