<div class="card pointsCard">

    <div class="nw_accordion">
        <h5 data-toggle="collapse" data-target="#collapse<?php echo $key; ?>" aria-expanded="true" aria-controls="collapse<?php echo $key; ?>">
            <?php echo $sectionName; ?>


            <?php
            if ( $include_points == 1 || !isset($include_points) ) {
                if ($sectionPoints > 0) {
                    //only print points if more than 0
                    echo '<span class="badge badge-light pull-right points-badge">' . $pre_points_message . ' ';
                    echo $sectionPoints;
                    echo ' ' . $post_points_message . '</span>';
                }
            }
            ?>



        </h5>
    </div>
    <?php if ($include_members) { ?>
    <div class="nw_panel panel">
        <div class="card-body">

            <?php
            foreach ($sectionMembers as $yp) {

                $nameDisplay = get_field('points_young_person_name_display', 'option');
                if ($nameDisplay == "id_only") {
                    $name = $yp['scout_id'];
                } else if ($nameDisplay == "firstName") {
                    $name = $yp['firstname'];
                } else if ($nameDisplay == "firstName+") {
                    $name = $yp['firstname'].' '.substr($yp['lastname'], 0, 2);
                } else if ($nameDisplay == "firstName+1") {
                    $name = $yp['firstname'].' '.substr($yp['lastname'], 0, 1);
                } else if ($nameDisplay == "lastName") {
                    $name = $yp['lastname'];
                } else if ($nameDisplay == "lastName+") {
                    $name = $yp['lastname'].' '.substr($yp['firstname'], 0, 2);
                } else if ($nameDisplay == "lastName+1") {
                    $name = $yp['lastname'].' '.substr($yp['firstname'], 0, 1);
                } else if ($nameDisplay == "initial") {
                    $name = substr($yp['firstname'], 0, 1) . " " . $yp['lastname'];
                } else if ($nameDisplay == "fullname") {
                    $name = $yp['lastname'].' '.$yp['firstname'];
                } else {
                    $name = $yp['firstname'].' '.$yp['lastname'];
                }

                ?>


                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            <i class="youngPersonIcon fa fa-user-circle fa-3x" aria-hidden="true" style="vertical-align: middle;"></i>
                            <?php echo $name; ?>

                            <?php
                            if ($include_rank) {
                                echo '<span class="badge badge-light badge-rank">';
                                echo $yp['patrol_role_level_label'];
                                echo '</span>';
                            }
                            ?>
                        </h5>
                    </div>
                </div>

            <?php } ?>

        </div>
    </div>
    <?php } ?>

</div>