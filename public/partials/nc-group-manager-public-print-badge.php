<?php
    $badgeSize = get_field('badge_size', 'options');
    if ($badgeSize == 0) {
        $badgeSize = 100;
    }
?>

<img src="<?php echo 'https://onlinescoutmanager.co.uk/' . $pictureUrl; ?>" alt="<?php echo $badgeLabel; ?> badge <?php echo $level; ?>" title="<?php echo $badgeLabel; ?> badge <?php echo $level; ?>" width="<?php echo $badgeSize; ?>" />

