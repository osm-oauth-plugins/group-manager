<li id="evening_<?php echo $eveningid; ?>" class="list-group-item">

    <?php

    //Our YYYY-MM-DD date.
    //Convert it into a timestamp.
    $timestamp = strtotime($meetingdate);

    $dateFormat = get_field("date_format", "options");
    $meetingdate = date($dateFormat, $timestamp);

    ?>

    <?php if ($datesOnly) { ?>
        <small><?php echo $meetingdate; ?> - <?php echo $title; ?></small>
    <?php } else { ?>
        <small><?php echo $meetingdate; ?>: <?php echo substr($meetingstart, 0, -3); ?> - <?php echo substr($meetingend, 0, -3); ?></small>
        <br/>
        <small><?php echo $title; ?></small>
    <?php } ?>
</li>