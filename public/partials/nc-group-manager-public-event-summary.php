<li id="event_<?php echo $eventid; ?>" class="list-group-item">


    <?php

    //Our YYYY-MM-DD date.
    //Convert it into a timestamp.
    $timestamp = strtotime($startdate);
    $timestamp = strtotime($enddate);

    $dateFormat = get_field("date_format_events", "options");
    $startdate = date($dateFormat, $timestamp);
    $enddate = date($dateFormat, $timestamp);

    ?>

    <?php if ($datesOnly) { ?>
        <small><?php echo $startdate; ?> - <?php echo $title; ?></small>
    <?php } else { ?>
        <small><?php echo $startdate; ?> <?php echo substr($eventstart, 0, -3); ?> - <?php echo $enddate; ?> <?php echo substr($eventend, 0, -3); ?></small>
        <br/>
        <small><?php echo $title; ?></small>
    <?php } ?>
</li>