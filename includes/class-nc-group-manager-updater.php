<?php

/**
 * Fired during plugin update
 *
 * @link       https://neoweb.co.uk
 * @since      1.0.0
 *
 * @package    Neoweb_Connector
 * @subpackage Neoweb_Connector/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's update.
 *
 * @since      1.0.0
 * @package    Neoweb_Connector
 * @subpackage Neoweb_Connector/includes
 * @author     Jaco Mare <jaco.mare@neoweb.co.uk>
 */
class NC_Group_Manager_Updater {

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @param $upgrader_object
     * @param $funcOptions
     * @since    1.0.0
     */
	public static function update($upgrader_object, $funcOptions) {

        if ( !function_exists( 'get_plugins' ) ){
            require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        }

        $plugin_data = get_option("neoweb-connector-group-manager");

        if( $funcOptions['action'] == 'update' && $funcOptions['type'] == 'plugin' && isset( $funcOptions['plugins'] ) ) {
            // Iterate through the plugins being updated and check if ours is there
            foreach( $funcOptions['plugins'] as $plugin ) {
                if( $plugin == $plugin_data['pluginName'] ) {

                    //Clear all old access tokens
                    update_option($plugin_data['pluginSlug'] . "_osm_access_token_data", null);
                    update_option($plugin_data['pluginSlug'] . "_osm_access_token_expiry", null);

                    $headers = "Content-Type: text/html; charset=UTF-8" .PHP_EOL;

                    $to = $plugin_data['supportEmail'];
                    $subject = "Site Updated:" . $plugin_data['pluginName'];

                    ob_start();

                    include(plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/register-plugin-email-template.php');
                    $htmlMessage = ob_get_clean();

                    wp_mail( $to, $subject, $htmlMessage, $headers);

                }
            }
        }
	}

}
