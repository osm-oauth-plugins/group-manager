<?php

/**
 * Fired during plugin activation
 *
 * @link       https://neoweb.co.uk
 * @since      1.0.0
 *
 * @package    Nc_Group_Manager
 * @subpackage Nc_Group_Manager/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Nc_Group_Manager
 * @subpackage Nc_Group_Manager/includes
 * @author     Jaco Mare <jaco.mare@neoweb.co.uk>
 */
class Nc_Group_Manager_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
        if ( !function_exists( 'get_plugins' ) ){
            require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        }

        $plugin_data = get_option('neoweb-connector-group-manager');

        //Clear token and old app data
        update_option($plugin_data['pluginSlug'] . "_osm_access_token_data", null);
        update_option($plugin_data['pluginSlug'] . "_osm_access_token_expiry", null);
        //update_field($plugin_data['pluginSlug'] . "_osm_oauth_client_id", "", "option");
        //update_field($plugin_data['pluginSlug'] . "_osm_oauth_secret", "", "option");

        $headers = "Content-Type: text/html; charset=UTF-8" .PHP_EOL;

        $to = $plugin_data['supportEmail'];
        $subject = "New Site Registered/Activated:" . $plugin_data['pluginName'];

        ob_start();

        include(plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/register-plugin-email-template.php');
        $htmlMessage = ob_get_clean();

        wp_mail( $to, $subject, $htmlMessage, $headers);
	}

}
