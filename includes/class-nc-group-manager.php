<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://neoweb.co.uk
 * @since      1.0.0
 *
 * @package    Nc_Group_Manager
 * @subpackage Nc_Group_Manager/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Nc_Group_Manager
 * @subpackage Nc_Group_Manager/includes
 * @author     Jaco Mare <jaco.mare@neoweb.co.uk>
 */
class Nc_Group_Manager {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Nc_Group_Manager_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected Nc_Group_Manager_Loader $loader;

    /**
     * @var array
     */
    private $plugin_data;

    /**
     * @param $key
     * @return string
     */
    public function getPluginData($key): string
    {
        return $this->plugin_data[$key];
    }


	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

        $this->plugin_data = get_option('neoweb-connector-group-manager');
        
		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Nc_Group_Manager_Loader. Orchestrates the hooks of the plugin.
	 * - Nc_Group_Manager_i18n. Defines internationalization functionality.
	 * - Nc_Group_Manager_Admin. Defines all hooks for the admin area.
	 * - Nc_Group_Manager_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-nc-group-manager-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-nc-group-manager-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-nc-group-manager-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-nc-group-manager-public.php';

		$this->loader = new Nc_Group_Manager_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Nc_Group_Manager_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Nc_Group_Manager_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Nc_Group_Manager_Admin();

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

        $this->loader->add_action( 'admin_menu', $plugin_admin, 'load_admin_pages');
        $this->loader->add_action( 'init', $plugin_admin, 'loadAdminPageFields');

        $ajaxCallers = new Nc_Group_Manager_Admin_Ajax_Callers();
        $this->loader->add_action( 'wp_ajax_trigger_log_refresh_' .  str_replace("-", "_", $this->getPluginData('pluginSlug')), $ajaxCallers, 'trigger_log_refresh' );
        $this->loader->add_action( 'wp_ajax_trigger_transient_log_refresh_' .  str_replace("-", "_", $this->getPluginData('pluginSlug')), $ajaxCallers, 'trigger_transient_log_refresh' );
        $this->loader->add_action( 'wp_ajax_trigger_cache_refresh_' .  str_replace("-", "_", $this->getPluginData('pluginSlug')), $ajaxCallers, 'trigger_cache_refresh' );
        $this->loader->add_action( 'wp_ajax_trigger_create_licence_key_request_' . str_replace("-", "_", $this->getPluginData('pluginSlug')), $ajaxCallers, 'trigger_create_licence_key_request' );
        $this->loader->add_action( 'wp_ajax_trigger_check_licence_key_request_' . str_replace("-", "_", $this->getPluginData('pluginSlug')), $ajaxCallers, 'trigger_check_licence_key_request' );
        $this->loader->add_action( 'wp_ajax_trigger_activate_licence_key_request_' . str_replace("-", "_", $this->getPluginData('pluginSlug')), $ajaxCallers, 'trigger_activate_licence_key_request' );
        $this->loader->add_action( 'wp_ajax_trigger_api_test_' .  str_replace("-", "_", $this->getPluginData('pluginSlug')), $ajaxCallers, 'trigger_api_test' );

        $this->loader->add_action( 'acf/save_post', $plugin_admin, 'trigger_publish_click_scripts', 20);
    }

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Nc_Group_Manager_Public();

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

        $registerShortCodes = new Nc_Group_Manager_Register_Plugin_Short_Codes();
        $this->loader->add_shortcode( 'OSM_Section_Points', $registerShortCodes, 'create_points_report' );
        $this->loader->add_shortcode( 'OSM-Section-Points', $registerShortCodes, 'create_points_report' );
        $this->loader->add_shortcode( 'OSM_Event_Summary', $registerShortCodes, 'fetch_osm_event_summary' );
        $this->loader->add_shortcode( 'OSM-Event-Summary', $registerShortCodes, 'fetch_osm_event_summary' );
        $this->loader->add_shortcode( 'OSM_Program_Summary', $registerShortCodes, 'fetch_osm_program_summary' );
        $this->loader->add_shortcode( 'OSM-Program-Summary', $registerShortCodes, 'fetch_osm_program_summary' );
        $this->loader->add_shortcode( 'OSM_Program', $registerShortCodes, 'fetch_osm_program' );
        $this->loader->add_shortcode( 'OSM-Program', $registerShortCodes, 'fetch_osm_program' );
        $this->loader->add_shortcode( 'OSM_badge', $registerShortCodes, 'fetch_osm_badge_data' );
        $this->loader->add_shortcode( 'OSM-badge', $registerShortCodes, 'fetch_osm_badge_data' );

    }

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}


	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Nc_Group_Manager_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader(): Nc_Group_Manager_Loader
    {
		return $this->loader;
	}

}
