<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://neoweb.co.uk
 * @since      1.0.0
 *
 * @package    Nc_Group_Manager
 * @subpackage Nc_Group_Manager/includes
 */

use utils\NeoWeb_Connector_Loggers;
use utils\Neoweb_Connector_Transient_Manager;

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Nc_Group_Manager
 * @subpackage Nc_Group_Manager/includes
 * @author     Jaco Mare <jaco.mare@neoweb.co.uk>
 */
class Nc_Group_Manager_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

        $plugin_data = get_option('neoweb-connector-group-manager');

        //Clear token on deactivation
        update_option($plugin_data['pluginSlug'] . "_osm_access_token_data", null);
        update_option($plugin_data['pluginSlug'] . "_osm_access_token_expiry", null);


        //Delete the contents of the osmDebug folder
        (new NeoWeb_Connector_Loggers())->recursiveRemove($plugin_data['pluginSlug']);

        //Disable the osmDebug option if active
        update_field($plugin_data['pluginSlug'] . '_enable_api_call_logs', 0, 'option');
        update_field($plugin_data['pluginSlug'] . '_enable_debug_logs', 0, 'option');

        $results = (new Neoweb_Connector_Transient_Manager($plugin_data['pluginSlug'] ))->wds_delete_transients();
	}

}
