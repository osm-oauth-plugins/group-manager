<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit6cc337b4a90ba21c70b54e30a9813cc7
{
    public static $classMap = array (
        'Nc_Group_Manager_Admin_Ajax_Callers' => __DIR__ . '/../../../..' . '/admin/admin-includes/class-nc-group-manager-admin-ajax-callers.php',
        'Nc_Group_Manager_Register_Plugin_Settings_Page_Badges' => __DIR__ . '/../../../..' . '/admin/admin-includes/class-nc-group-manager-register-plugin-settings-page-badges.php',
        'Nc_Group_Manager_Register_Plugin_Settings_Page_Events' => __DIR__ . '/../../../..' . '/admin/admin-includes/class-nc-group-manager-register-plugin-settings-page-events.php',
        'Nc_Group_Manager_Register_Plugin_Settings_Page_Points' => __DIR__ . '/../../../..' . '/admin/admin-includes/class-nc-group-manager-register-plugin-settings-page-points.php',
        'Nc_Group_Manager_Register_Plugin_Settings_Page_Programme' => __DIR__ . '/../../../..' . '/admin/admin-includes/class-nc-group-manager-register-plugin-settings-page-programme.php',
        'Nc_Group_Manager_Register_Plugin_Short_Codes' => __DIR__ . '/../../../..' . '/admin/admin-includes/class-nc-group-manager-register-plugin-short-codes.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->classMap = ComposerStaticInit6cc337b4a90ba21c70b54e30a9813cc7::$classMap;

        }, null, ClassLoader::class);
    }
}
