<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://neoweb.co.uk
 * @since             1.0.0
 * @package           NC_Group_Manager
 *
 * @wordpress-plugin
 * Product Name:            NeoWeb Connector
 * Product Slug:            neoweb-connector
 * Plugin Name:             NeoWeb Connector Group Manager
 * Plugin URI:              https://scoutsuk.org
 * Description:             Online Scout Manager (OSM) WordPress Connector allowing groups to display section data including, programme, event, badge and patrol data on your website.
 * Version:                 1.3
 * Required Version:        1.3
 * Release Date:            17/08/2021
 * Requires at least:       5.6
 * Requires PHP:            7.4
 * Author:                  Jaco Mare
 * Author URI:              https://neoweb.co.uk
 * Copyright:               Jaco Mare
 * Text Domain:             neoweb-connector-group-manager
 * Domain Path:             /languages
 * Support Email:           support@neoweb.co.uk
 * BitBucket Support Link:  https://bitbucket.org/osm-oauth-plugins/group-manager/issues?status=new&status=open
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

update_option('neoweb-connector-group-manager', get_file_data(__FILE__, array(
    'productName'               => 'Product Name',
    'productSlug'               => 'Product Slug',
    'pluginName'                => 'Plugin Name',
    'pluginSlug'                => 'Text Domain',
    'pluginVersion'             => 'Version',
    'requiredProductVersion'    => 'Required Version',
    'pluginReleaseDate'         => 'Release Date',
    'supportEmail'              => 'Support Email',
    'supportLink'               => 'BitBucket Support Link'
), false));

update_option("neoweb-connector-group-manager-path", plugin_dir_path( __FILE__ ));
update_option("neoweb-connector-group-manager-url", plugin_dir_url( __FILE__ ));

$plugin_data = get_option('neoweb-connector-group-manager');
require_once plugin_dir_path( __FILE__ ) . 'includes/libs/pluginUpdateChecker/plugin-update-checker.php';
$pluginUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://scoutsuk.org/plugin-updater/'.$plugin_data['pluginSlug'].'.json',
    __FILE__,
    $plugin_data['pluginSlug']
);

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-nc-group-manager-activator.php
 */
function activate_nc_group_manager() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-nc-group-manager-activator.php';
	Nc_Group_Manager_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-nc-group-manager-deactivator.php
 */
function deactivate_nc_group_manager() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-nc-group-manager-deactivator.php';
	Nc_Group_Manager_Deactivator::deactivate();
}

/**
 * The code that runs during plugin update.
 * This action is documented in includes/class-neoweb-connector-updater.php
 *
 * @param $upgrader_object
 * @param $funcOptions
 */
function update_nc_group_manager($upgrader_object, $funcOptions) {
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-nc-group-manager-updater.php';
    NC_Group_Manager_Updater::update($upgrader_object, $funcOptions);
}
add_action( 'upgrader_process_complete', 'update_nc_group_manager', 10, 2);

register_activation_hook( __FILE__, 'activate_nc_group_manager' );
register_deactivation_hook( __FILE__, 'deactivate_nc_group_manager' );

function nc_group_manager_detect_plugin_deactivation( $plugin, $network_activation ) {
    if ($plugin=="neoweb-connector/neoweb-connector.php")
    {
        require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        deactivate_plugins(__FILE__);
        wp_die( __(
            'Please reactivate the NeoWeb Connector plugin before continuing.',
            'neoweb-connector' ),
            'Plugin dependency check',
            array( 'back_link' => true ) );
    }
}
add_action( 'deactivated_plugin', 'nc_group_manager_detect_plugin_deactivation', 10, 2 );

/**
 * Load all 3rd party dependencies and classes before we start
 */
require_once plugin_dir_path(__FILE__ ) . 'includes/libs/3rd-party/autoload.php';

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-nc-group-manager.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_nc_group_manager() {
    include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    if( !is_plugin_active( 'neoweb-connector/neoweb-connector.php') ) {
        deactivate_plugins(__FILE__);
        wp_die( __(
            'Please install and activate the NeoWeb Connector plugin before continuing.',
            'neoweb-connector' ),
            'Plugin dependency check',
            array( 'back_link' => true ) );
    } else {
        $plugin = new Nc_Group_Manager();
        $plugin->run();
    }
}
run_nc_group_manager();
