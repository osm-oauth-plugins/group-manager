<?php
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_licence_neoweb-connector-group-manager',
        'title' => "NeoWeb Connector - Group Manager - Version: $pluginVersion",
        'fields' => array(
            array(
                'key' => 'neoweb-connector-group-manager_txn_id',
                'label' => 'PayPal transaction ID',
                'name' => 'neoweb-connector-group-manager_txn_id',
                'type' => 'text',
                'instructions' => 'Please click Save Personal Details before Requesting a licence key as the request will fail.',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'neoweb-connector-group-manager_licence_key',
                'label' => 'Licence Key',
                'name' => 'neoweb-connector-group-manager_licence_key',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'readonly' => 1,
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'neoweb-connector-group-manager_licence_status',
                'label' => 'Licence Status',
                'name' => 'neoweb-connector-group-manager_licence_status',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'readonly' => 1,
                'default_value' => '',
                'placeholder' => 'Active',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'neoweb-connector-group-manager_request_licence',
                'label' => 'Request & Activate Licence Key',
                'name' => '',
                'type' => 'message',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'neoweb-connector-group-manager_licence_key',
                            'operator' => '==empty',
                        ),
                        array(
                            'field' => 'neoweb-connector-group-manager_txn_id',
                            'operator' => '!=empty',
                        ),
                        array(
                            'field' => 'neoweb-connector_saved',
                            'operator' => '==',
                            'value' => 'Yes',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '<button type="button" class="button-secondary" id="neoweb_connector_group_manager_register">Request & Activate Licence Key</button>',
                'new_lines' => 'wpautop',
                'esc_html' => 0,
            ),
            array(
                'key' => 'neoweb-connector-group-manager_check_licence',
                'label' => 'Check Licence Key',
                'name' => '',
                'type' => 'message',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'neoweb-connector-group-manager_licence_key',
                            'operator' => '!=empty',
                        ),
                        array(
                            'field' => 'neoweb-connector-group-manager_licence_status',
                            'operator' => '==',
                            'value' => 'Active',
                        ),
                        array(
                            'field' => 'neoweb-connector-group-manager_licence_status',
                            'operator' => '!=',
                            'value' => 'Expired',
                        ),
                        array(
                            'field' => 'neoweb-connector-group-manager_licence_status',
                            'operator' => '!=',
                            'value' => 'Blocked',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '<button type="button" class="button-secondary" id="neoweb_connector_group_manager_check">Check Licence Key</button>',
                'new_lines' => 'wpautop',
                'esc_html' => 0,
            ),
            array(
                'key' => 'neoweb-connector-group-manager_activate_licence',
                'label' => 'Activate Licence Key',
                'name' => '',
                'type' => 'message',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'neoweb-connector-group-manager_licence_key',
                            'operator' => '!=empty',
                        ),
                        array(
                            'field' => 'neoweb-connector-group-manager_licence_status',
                            'operator' => '!=',
                            'value' => 'Active',
                        ),
                        array(
                            'field' => 'neoweb-connector-group-manager_licence_status',
                            'operator' => '!=',
                            'value' => 'Expired',
                        ),
                        array(
                            'field' => 'neoweb-connector-group-manager_licence_status',
                            'operator' => '!=',
                            'value' => 'Blocked',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '<button type="button" class="button-secondary" id="neoweb_connector_group_manager_activate">Activate Licence Key</button>',
                'new_lines' => 'wpautop',
                'esc_html' => 0,
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'neoweb-connector-licences',
                ),
            ),
        ),
        'menu_order' => 10,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'left',
        'instruction_placement' => 'field',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));

endif;