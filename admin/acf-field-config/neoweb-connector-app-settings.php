<?php

if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_neoweb-connector-group-manager_application_fields',
        'title' => 'Group Manager - Application Settings',
        'fields' => array(
            array(
                'key' => 'neoweb-connector_install_instructions-group',
                'label' => '',
                'name' => '',
                'type' => 'message',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => false,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '<div class="wrap">
<ol>
<li>Navigate to <a href="https://www.onlinescoutmanager.co.uk/" target="_blank" rel="noopener">OSM</a></li>
<li>Login using your main login details</li>
<li>
<p>Click on "Settings" link</p>
<img src="'.$pluginImagePath.'/osm-settings.png" /></li>
<li>
<p>Click on "My Account Details" link</p>
<img src="'.$pluginImagePath.'/osm-settings-my-account-details.png" /></li>
<li>
<p>Click on "Developer Tools" link</p>
<img src="'.$pluginImagePath.'/osm-settings-developer-tools.png" /></li>
<li>
<p>Click on the "Create Application" button</p>
<img src="'.$pluginImagePath.'/osm-settings-create-application.png" /></li>
<li>
<p>IMPORTANT: Enter a unique name per plugin in the NeoWeb Connector Suite - for example <strong>"'.$pluginSlug.'"</strong>, and click save</p>
<img src="'.$pluginImagePath.'/osm-settings-application-name.png" /></li>
<li>
<p>Copy the OAuth Client ID and OAuth Secret values and paste the values into relevant fields on the right, and then click close</p>
<p class="neowebWarning">These values need to be unique for each plugin in the NeoWeb Connector Suite</p>
<img style="width: 100%;" src="'.$pluginImagePath.'/osm-settings-oAuth-settings.png" /></li>
<li>
<p>Click on the application name you used in step 7 above</p>
<img src="'.$pluginImagePath.'/osm-settings-application-settings.png" /></li>
<li>
<p>IMPORTANT: Enter the following redirect URL, and then click save</p>
<p class="neowebWarning">'.$osmRedirectUrl.'</p>
<img src="'.$pluginImagePath.'/osm-settings-redirect-urls.png" /></li>
<li>
<p>Click the "Authenticate Plugin" button to the right.</p>
<p>If the authentication has been completed successfully you should be presented with the following message: OSM has been successfully authenticated.</p>
</li>
</ol>
</div>',
                'new_lines' => 'wpautop',
                'esc_html' => 0,
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'neoweb-connector-group-manager-app-settings',
                ),
            ),
        ),
        'menu_order' => 10,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));

endif;

if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_neoweb-connector-group-manager_application_fields_side',
        'title' => 'Group Manager - Application Settings',
        'fields' => array(
            array(
                'key' => 'neoweb-connector-group-manager_osm_oauth_notice',
                'label' => '',
                'name' => '',
                'type' => 'message',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => false,
                'wrapper' => array(
                    'width' => '',
                    'class' => 'neowebWarning',
                    'id' => '',
                ),
                'message' => 'Please follow the instructions on the left to obtain these values from OSM.',
                'new_lines' => 'wpautop',
                'esc_html' => 0,
            ),
            array(
                'key' => 'neoweb-connector-group-manager_osm_oauth_client_id',
                'label' => 'OSM oAuth Client ID',
                'name' => 'neoweb-connector-group-manager_osm_oauth_client_id',
                'type' => 'text',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'neoweb-connector-group-manager_osm_oauth_secret',
                'label' => 'OSM oAuth Secret',
                'name' => 'neoweb-connector-group-manager_osm_oauth_secret',
                'type' => 'text',
                'instructions' => '',
                'required' => 1,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'neoweb-connector-group-permissions',
                'label' => 'OSM Permissions Required',
                'name' => 'neoweb-connector-group-permissions',
                'type' => 'message',
                'instructions' => 'We will be using these permissions to authenticate with OSM',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => 'Admin - None<br/> Badge Records - Read Only<br/> Events - Read Only<br/> Finance - None<br/> Flexi Records - None<br/> Member - Read Only<br/> Programme - Read Only<br/> Quartermaster - None'
            ),
            array(
                'key' => 'neoweb-connector-group-manager_apitest_accordion',
                'label' => 'API Test',
                'name' => '',
                'type' => 'accordion',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'neoweb-connector-group-manager_osm_oauth_client_id',
                            'operator' => '!=empty',
                        ),
                        array(
                            'field' => 'neoweb-connector-group-manager_osm_oauth_secret',
                            'operator' => '!=empty',
                        ),
                    ),
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'open' => 0,
                'multi_expand' => 0,
                'endpoint' => 0,
            ),
            array(
                'key' => 'neoweb-connector-group-manager_maxratelimit',
                'label' => 'Maximum API calls per cycle:',
                'name' => 'neoweb-connector-group-manager_maxRateLimit',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'readonly' => 1,
                'message' => '',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'neoweb-connector-group-manager_currentratelimit',
                'label' => 'Remaining API calls this cycle:',
                'name' => 'neoweb-connector-group-manager_currentRateLimit',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'readonly' => 1,
                'message' => '',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'neoweb-connector-group-manager_ratelimitrefresh',
                'label' => 'Next cycle refresh:',
                'name' => 'neoweb-connector-group-manager_rateLimitRefresh',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'readonly' => 1,
                'message' => '',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'neoweb-connector-group-manager_test_api',
                'label' => 'Test API',
                'name' => '',
                'type' => 'message',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '<button type="button" class="button-secondary" id="neoweb_connector_group_manager_trigger_APITest">Test OSM API</button>',
                'new_lines' => 'wpautop',
                'esc_html' => 0,
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'neoweb-connector-group-manager-app-settings',
                ),
            ),
        ),
        'menu_order' => 10,
        'position' => 'side',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));

endif;