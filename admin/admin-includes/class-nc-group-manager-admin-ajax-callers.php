<?php

use utils\NeoWeb_Connector_Admin_Notifications;
use utils\NeoWeb_Connector_Auth_Caller;
use utils\NeoWeb_Connector_Licence_Manager;
use utils\NeoWeb_Connector_Loggers;
use utils\Neoweb_Connector_Transient_Manager;

class Nc_Group_Manager_Admin_Ajax_Callers
{
    /**
     * @var array
     */
    private $plugin_data;

    /**
     * @param $key
     * @return string
     */
    public function getPluginData($key): string
    {
        return $this->plugin_data[$key];
    }
    
    /**
     * __constructor.
     */
    public function __construct()
    {
        $this->plugin_data = get_option('neoweb-connector-group-manager');
    }

    /**
     *
     */
    public function trigger_create_licence_key_request() {

        $firstName = get_field($this->getPluginData('productSlug') . '_first_name', 'option');
        $lastName = get_field($this->getPluginData('productSlug') . '_last_name', 'option');
        $email = get_field($this->getPluginData('productSlug') . '_email_address', 'option');
        $txn_id = get_field($this->getPluginData('pluginSlug') . '_txn_id', 'option');
        $scout_groupdistrictcounty = get_field($this->getPluginData('productSlug') . '_organisation', 'option');

        if ($firstName && $lastName && $email && $txn_id) {
            //We have valid personal details lets create a new licence key
            $licenceRequestBody = array (
                'secret_key' => '',
                'slm_action' => 'slm_create_new',
                'first_name' => $firstName,
                'last_name' => $lastName,
                'email' => $email,
                'txn_id' => $txn_id,
                'company_name' => $scout_groupdistrictcounty,
                'max_allowed_domains' => '1',
                'date_created' =>date("Y-m-d"),
                'date_expiry' => '',
                'product_ref' => urlencode($this->getPluginData('pluginSlug'))

            );

            $licenceRequestResponse = (new NeoWeb_Connector_Licence_Manager($this->getPluginData('pluginSlug')))->createLicenceKey($licenceRequestBody);

            // Check for error in the response
            if (is_wp_error($licenceRequestResponse)){
                (new NeoWeb_Connector_Admin_Notifications())->add_flash_notice("Unexpected Error! The query returned with an error.",
                    "error", false);
            }

            // License data.
            $license_data = json_decode(wp_remote_retrieve_body($licenceRequestResponse));

            if(isset($license_data->result) && $license_data->result == 'success'){

                update_field($this->getPluginData('pluginSlug') . '_licence_key', $license_data->key, "option");

                $licenceStatus = (new NeoWeb_Connector_Licence_Manager($this->getPluginData('pluginSlug')))->getLicenceStatus();

                update_field($this->getPluginData('pluginSlug') . '_licence_status', $licenceStatus, "option");

                (new NeoWeb_Connector_Admin_Notifications())->add_flash_notice($license_data->message,
                    "success", false);

            } else {

                (new NeoWeb_Connector_Admin_Notifications())->add_flash_notice($license_data->message,
                    "error", false);

            }
        } else {
            //We do not have valid personal details, throw and error
            (new NeoWeb_Connector_Admin_Notifications())->add_flash_notice("You have not completed your personal details. Please complete all 
				mandatory fields and click 'Save personal details' before requesting a licence key",
                "error", false);
        }
    }

    private string $licenceValidationKey = '60044807dd1d40.88556940';
    public function trigger_check_licence_key_request(): bool
    {
        $licenceRequestBody = array(
            'slm_action' => 'slm_check',
            'secret_key' => $this->licenceValidationKey,
            'license_key' => get_field($this->getPluginData('pluginSlug') . '_licence_key', 'option'),
            'registered_domain' => get_site_url()
        );

        $licenceRequestResponse = (new NeoWeb_Connector_Licence_Manager($this->getPluginData('pluginSlug')))->activateLicenceKey($licenceRequestBody);

        $license_data = json_decode(wp_remote_retrieve_body($licenceRequestResponse), true);
        if (isset($license_data) && $license_data['result'] === 'success') {

            $licenceStatus = $license_data['status'];
            $registeredDomains = $license_data['registered_domains'];

            $domainCheck = false;
            foreach ($registeredDomains as $registered_domain) {
                if ($registered_domain['registered_domain'] == get_site_url()) {
                    $domainCheck = true;
                }
            }

            if ($licenceStatus == "active" && $domainCheck) {
                $currentStatus = get_field($this->getPluginData('pluginSlug') . '_licence_status', 'option');
                //The status has been changed lest update the admin screens
                if ($currentStatus != $licenceStatus)
                    update_field($this->getPluginData('pluginSlug') . '_licence_status', ucfirst($licenceStatus), "option");
                $response = true;
            } else {
                //The licence key has been cancelled...
                update_field($this->getPluginData('pluginSlug') . '_licence_status', ucfirst($licenceStatus), "option");
                delete_transient('neoweb_connector_' . $this->getPluginData('pluginSlug'));
                $response = false;
            }
        } else {
            if (isset($license_data['error_code']) && $license_data['error_code'] == 60) {
                //The licence key we have is invalid or does not exists, remove it so that we can try again
                update_field($this->getPluginData('pluginSlug') . '_licence_key', "", "option");
                update_field($this->getPluginData('pluginSlug') . '_licence_status', "Please request a licence key from NeoWeb using the button below...", "option");
            }
            $response = false;
        }

        wp_send_json($response);
        wp_die();

    }

    public function trigger_activate_licence_key_request() {

        $licenceRequestBody = array(
            'slm_action' => 'slm_activate',
            'secret_key' => '',
            'license_key' => '',
            'registered_domain' => get_site_url()
        );

        $licenceRequestResponse = (new NeoWeb_Connector_Licence_Manager($this->getPluginData('pluginSlug')))->activateLicenceKey($licenceRequestBody);

        // Check for error in the response
        if (is_wp_error($licenceRequestResponse)){
            (new NeoWeb_Connector_Admin_Notifications())->add_flash_notice("Unexpected Error! The query returned with an error.",
                "error", false);
        }

        $license_data = json_decode(wp_remote_retrieve_body($licenceRequestResponse));
        if($license_data->result == 'success'){//Success was returned for the license activation
            $licenceStatus = (new NeoWeb_Connector_Licence_Manager($this->getPluginData('pluginSlug')))->getLicenceStatus();
            update_field($this->getPluginData('pluginSlug') . '_licence_status', $licenceStatus, "option");

            (new NeoWeb_Connector_Admin_Notifications())->add_flash_notice($license_data->message,
                "success", false);

        }
        else{
            //Show error to the user. Probably entered incorrect license key.
            (new NeoWeb_Connector_Admin_Notifications())->add_flash_notice($license_data->message,
                "error", false);
            /*
            if (strpos( $license_data->message, 'Reached maximum activation' ) > 0) {
                update_field($this->getPluginData('pluginSlug') . '_licence_key', "", "option");
                update_field($this->getPluginData('pluginSlug') . '_licence_status', "Activation Failed - Request a new key", "option");
            }*/
        }
    }

    public function trigger_log_refresh() {
        //Delete the contents of the osmDebug folder
        (new NeoWeb_Connector_Loggers())->recursiveRemove($this->getPluginData('pluginSlug'), "osmDebugLogs");
        (new NeoWeb_Connector_Admin_Notifications())->add_flash_notice("Debug log cleared successfully.",
            "success", false);

    }

    public function trigger_transient_log_refresh() {
        //Delete the contents of the osmDebugTransient Folder folder
        (new NeoWeb_Connector_Loggers())->recursiveRemove($this->getPluginData('pluginSlug'), "osmTransientDebugLogs");
        (new NeoWeb_Connector_Admin_Notifications())->add_flash_notice("Transient debug log cleared successfully.",
            "success", false);

    }

    public function trigger_cache_refresh() {
        if (get_field($this->getPluginData('pluginSlug') . '_enable_debug_logs', 'option')) {
            //Delete all transients
            (new NeoWeb_Connector_Loggers())->debug_logger( $this->getPluginData('productSlug'), "Transient CACHE clear started.. " );
        }
        $results = (new Neoweb_Connector_Transient_Manager($this->getPluginData('pluginSlug')))->wds_delete_transients();
        (new NeoWeb_Connector_Admin_Notifications())->add_flash_notice("WordPress transient cache cleared successfully. Total number of records found: " . $results['total'] . ' Number of records deleted successfully: ' . $results['deleted'],
            "success", false);
    }

    public function trigger_api_test() {
        $response = (new NeoWeb_Connector_Auth_Caller($this->plugin_data))->testCallAPI(
            "https://www.onlinescoutmanager.co.uk/api.php?action=getUserRoles"
        );
        wp_send_json($response);
        wp_die();
    }
    
}