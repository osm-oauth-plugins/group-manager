<?php


use utils\NeoWeb_Connector_Auth_Caller;
use utils\NeoWeb_Connector_Loggers;
use utils\NeoWeb_Connector_OSM_End_Points;

class Nc_Group_Manager_Register_Plugin_Short_Codes
{
    private array $plugin_data;

    /**
     * @param $key
     *
     * @return string
     */
    public function get_plugin_data($key): string {
        return $this->plugin_data[$key];
    }
    
    /**
     * @return array
     */
    public function getAllPluginData(): array {
        return $this->plugin_data;
    }

    /**
     * __constructor
     *
     * @since    1.0.0
     */
    public function __construct() {
        $this->plugin_data = get_option('neoweb-connector-group-manager');
    }

    public function create_points_report($attr) {
        $sectionID = $attr['sectionid'];
        $currentTermID = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->getCurrentTermID($sectionID);
        $html = "";
        if ($currentTermID != "") {
            $include_members = get_field('show_young_people', 'option');
            $include_points = get_field('show_points', 'option');
            $pre_points_message = get_field('text_to_show_before_points', 'option');
            $post_points_message = get_field('text_to_show_after_points', 'option');
            $include_rank = get_field('show_young_persons_rank', 'option');

            $url = NeoWeb_Connector_OSM_End_Points::getPatrolPoints;
            $formattedURL = (new NeoWeb_Connector_OSM_End_Points)->formatEndPoint($url, $sectionID, $currentTermID);

            $transientID = 'sectionPointsData_' . $sectionID;
            $patrolData = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->osmAPICaller($transientID, $formattedURL, 48, array());

            ob_start();
            $html = ob_get_clean();
            $html .= '<div id="accordionPoints" role="tablist">';
            foreach ($patrolData as $key=>$sub_section) {
                if (array_key_exists('patrolid', $sub_section) && $sub_section['patrolid'] > 0) { //Normal state active
                    $sectionName = $sub_section['name'];
                    $sectionPoints = $sub_section['points'];
                    $sectionMembers = $sub_section['members'];
                    ob_start();
                    include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-section-points.php');
                    $html .= ob_get_clean();
                } else if (array_key_exists('status', $sub_section) && $sub_section['status'] == false) { //Error State active - sample maintenance mode
                    $errorMsg = $sub_section['error']['message'];
                    ob_start();
                    include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-error-message.php');
                    $html .= ob_get_clean();
                }
            }
            $html .= "</div>";
        } else {
            ob_start();
            include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-error-no-active-term.php');
            $html .= ob_get_clean();
        }

        $html .= '<p style="font-size: 12px; margin-top: 10px;" class="text-right"><small>' . (new Neoweb_Connector_Public())->showSomeLove() .'</small></p>';

        return $html;

    }

    public function fetch_osm_event_summary($attr) {
        $sectionID = $attr['sectionid'];
        $currentTermID = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->getCurrentTermID($sectionID);
        $html = "";
        if ($currentTermID != "") {
            $url = NeoWeb_Connector_OSM_End_Points::getEventSummary;
            $formattedURL = (new NeoWeb_Connector_OSM_End_Points)->formatEndPoint($url, $sectionID, $currentTermID);

            $transientID = 'eventSummaryData_' . $sectionID;
            $eventSummaryData = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->osmAPICaller($transientID, $formattedURL, 12, array());

            if (isset($eventSummaryData['events'])) {
                ob_start();
                $html = ob_get_clean();

                $html .= '<ul class="list-group eventSummary">';
                foreach ($eventSummaryData['events'] as $event) {

                    $url = NeoWeb_Connector_OSM_End_Points::getStructureForEvent;
                    $formattedURL = (new NeoWeb_Connector_OSM_End_Points)->formatEndPoint($url, $sectionID, $event['eventid']);
                    $transientID = 'eventStructureData_' . $sectionID . '_' . $event['eventid'];

                    $eventData = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->osmAPICaller($transientID, $formattedURL, 48, array());

                    $eventid = $eventData['eventid'];
                    $startdate = $eventData['startdate'];
                    $enddate = $eventData['enddate'];
                    $eventstart = $eventData['starttime'];
                    $eventend = $eventData['endtime'];
                    $title = $eventData['name'];
                    $datesOnly = get_field("show_event_dates_only", "options");

                    ob_start();
                    include( get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-event-summary.php' );
                    $html .= ob_get_clean();
                }
                $html .= '</ul>';
            } else {
                $html = '<div class="alert alert-info" role="alert">
                    There is no events available.
                </div>';
            }

        } else {
            ob_start();
            include( get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-error-no-active-term.php');
            $html .= ob_get_clean();
        }

        $html .= '<p style="font-size: 12px; margin-top: 10px;" class="text-right"><small>'. (new Neoweb_Connector_Public())->showSomeLove() .'</small></p>';

        return $html;
    }

    public function fetch_osm_program_summary($attr) {
        $sectionID = $attr['sectionid'];
        $currentTermID = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->getCurrentTermID($sectionID);
        $html = "";
        if ($currentTermID != "") {
            $url = NeoWeb_Connector_OSM_End_Points::getProgramSummary;
            $formattedURL = (new NeoWeb_Connector_OSM_End_Points)->formatEndPoint($url, $sectionID, $currentTermID);

            $transientID = 'programSummaryData_' . $sectionID;

            $programSummaryData = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->osmAPICaller($transientID, $formattedURL, 48, array());

            if (isset($programSummaryData['items'])) {
                if (count($programSummaryData['items'])) {
                    ob_start();
                    $html = ob_get_clean();

                    $html .= '<ul class="meetingSummary list-group">';
                    foreach ($programSummaryData['items'] as $night) {
                        $eveningid = $night['eveningid'];
                        $datesOnly = get_field('show_dates_only', 'option');
                        $meetingdate = $night['meetingdate'];
                        $meetingstart = $night['starttime'];
                        $meetingend = $night['endtime'];
                        $title = $night['title'];
                        ob_start();
                        include( get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-meeting-summary.php' );
                        $html .= ob_get_clean();
                    }
                    $html .= '</ul>';
                } else {
                    $html = '<div class="alert alert-info" role="alert">
                    There is no meetings available.
                </div>';
                }
            } else {
                $html = '<div class="alert alert-info" role="alert">
                    There is no meetings available.
                </div>';
            }

        } else {
            ob_start();
            include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-error-no-active-term.php');
            $html .= ob_get_clean();
        }
        $html .= '<p style="font-size: 12px; margin-top: 0; padding: 5px 10px;" class="text-right"><small>'. (new Neoweb_Connector_Public())->showSomeLove() .'</small></p>';

        return $html;
    }

    public function fetch_osm_program($attr) {
        $sectionID = $attr['sectionid'];
        $currentTermID = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->getCurrentTermID($sectionID);

        $html = "";
        ob_start();
        if ($currentTermID != "") {
            $futureOnly = get_field('show_future_dates_only', 'option');
            $datesOnly = get_field('show_dates_only', 'option');
            //$allowCalDownload = get_field('show_calendar_download', 'option');
            $showParentHelperCount = get_field('show_number_of_parent_helpers_required', 'option');
            $showParentHelpNotes = get_field('show_notes_for_parent_helpers', 'option');
            $showParentNotes = get_field('show_notes_for_parents', 'option');
            $showGameNotes = get_field('show_game_notes', 'option');
            $showPreNotes = get_field('show_pre_meeting_notes', 'option');
            $showPostNotes = get_field('show_post_meeting_notes', 'option');
            $showLeaderNotes = get_field('show_leader_notes', 'option');
            $showBadges = get_field('show_linked_badges', 'option');

            $url = NeoWeb_Connector_OSM_End_Points::getProgramSummary;
            $formattedURL = (new NeoWeb_Connector_OSM_End_Points)->formatEndPoint($url, $sectionID, $currentTermID);

            $transientID = 'programSummaryData_' . $sectionID;

            $programSummaryData = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->osmAPICaller($transientID, $formattedURL, 48, array());


            $html .= '<div id="accordionMeeting" role="tablist">';
            if ( ! empty($programSummaryData['items']) ) { //Normal state active
                foreach ($programSummaryData['items'] as $night) {
                    if ($futureOnly == 1) {
                        if (strtotime($night["meetingdate"]) >= strtotime("now")) {
                            ob_start();
                            include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-program-overview.php');
                            $html .= ob_get_clean();
                        }
                    } else {
                        ob_start();
                        include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-program-overview.php');
                        $html .= ob_get_clean();
                    }
                }
            } else if ( empty($programSummaryData['items']) ) {
                $html .= '<div class="alert alert-info" role="alert">
                    There is no further meetings available.
                </div>';
            } else {
                $errorMsg = $programSummaryData['error']['message'];
                ob_start();
                include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-error-message.php');
                $html .= ob_get_clean();
            }
            $html .= '</div>';
        } else {
            ob_start();
            include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-error-no-active-term.php');
            $html .= ob_get_clean();
        }

        $html .= '<p style="font-size: 12px; margin-top: 10px;" class="text-right"><small>'. (new Neoweb_Connector_Public())->showSomeLove() .'</small></p>';
        $html .= ob_get_clean();
        return $html;
    }

    public function fetch_osm_badge_data($attr) {

        $sectionType = $attr['section'];
        $sectionID = $attr['sectionid'];
        $currentTermID = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->getCurrentTermID($sectionID);
        $html = "";
        if ($currentTermID != "") {
            $url = NeoWeb_Connector_OSM_End_Points::getBadgesByPerson;
            $formattedURL = (new NeoWeb_Connector_OSM_End_Points)->formatEndPoint($url, $sectionID, $currentTermID, $sectionType);

            $transientID = 'badgeDataByPerson_' . $sectionID;
            $badgeProgressData = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->osmAPICaller($transientID, $formattedURL, 48, array());

            $badgeRecord = array();

            $counter = 0;
            if ( ! empty($badgeProgressData['data']) ) { //Normal state active
                foreach ($badgeProgressData['data'] as $yp) {
                    $nameDisplay = get_field('young_person_name_display', 'option');

                    if ($nameDisplay == "id_only") {
                        $badgeRecord[$counter]["name"] = $yp['scout_id'];
                    } else if ($nameDisplay == "firstName") {
                        $badgeRecord[$counter]["name"] = $yp['firstname'];
                    } else if ($nameDisplay == "firstName+") {
                        $badgeRecord[$counter]["name"] = $yp['firstname'] . ' ' . substr($yp['lastname'], 0, 2);
                    } else if ($nameDisplay == "firstName+1") {
                        $badgeRecord[$counter]["name"] = $yp['firstname'] . ' ' . substr($yp['lastname'], 0, 1);
                    } else if ($nameDisplay == "lastName") {
                        $badgeRecord[$counter]["name"] = $yp['lastname'];
                    } else if ($nameDisplay == "lastName+") {
                        $badgeRecord[$counter]["name"] = $yp['lastname'] . ' ' . substr($yp['firstname'], 0, 2);
                     } else if ($nameDisplay == "lastName+1") {
                        $badgeRecord[$counter]["name"] = $yp['lastname'] . ' ' . substr($yp['firstname'], 0, 1);
                    } else if ($nameDisplay == "fullname") {
                        $badgeRecord[$counter]["name"] = $yp['lastname'].' '.$yp['firstname'];
                    } else if ($nameDisplay == "initial") {
                        $badgeRecord[$counter]["name"] = substr($yp['firstname'], 0, 1).' '.$yp['lastname'];
                    } else {
                        $badgeRecord[$counter]["name"] = $yp['firstname'] . ' ' . $yp['lastname'];
                    }

                    $badgeRecord[$counter]["section"] = $sectionType;
                    $badgeRecord[$counter]["lodge"] = $yp['patrol'];
                    $badgeRecord[$counter]["lodge_level"] = $yp['patrol_role_level_label'];
                    $badgeRecord[$counter]["scoutID"] = $yp['scout_id'];
                    $badgeRecord[$counter]["age"] = $yp['age'];
                    $badgeRecord[$counter]["badgeRecords"] = $yp['badges'];
                    $counter++;
                }

                $badgetype = $attr['badgetype'];
                $showProgress = $attr['show-progress'];
                $showAwarded = $attr['show-awarded'];
                $show_lodge_badge = get_field('show_lodgepackpatrol_badge', 'option');
                $show_rank_badge = get_field('show_lodgepackpatrol_rank', 'option');

                ob_start();
                $html .= ob_get_clean();

                $html .= '<div id="badgeListAccordion" role="tablist">';
                foreach ($badgeRecord as $yp) {
                    $name = $yp['name'];
                    $lodge = $yp['lodge'];
                    $lodge_level = $yp['lodge_level'];
                    $scoutID = $yp['scoutID'];
                    $badgeRecords = $yp['badgeRecords'];
                    ob_start();
                    include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-badge-progress-list.php');
                    $html .= ob_get_clean();

                }
                $html .= '</div>';
            } else {
                $errorMsg = $badgeProgressData['error']['message'];
                ob_start();
                include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-error-message.php');
                $html .= ob_get_clean();
            }

        } else {
            ob_start();
            include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-error-no-active-term.php');
            $html .= ob_get_clean();
        }

        $html .= '<p style="font-size: 12px; margin-top: 10px;" class="text-right"><small>' . (new Neoweb_Connector_Public())->showSomeLove() . '</small></p>';

        return $html;
    }

    private function fetch_osm_program_data( $sectionID, $currentTermID, $eveningID ) {

        $plugin_folder_path = plugin_dir_url( __FILE__ );

        $domain = 'https://www.onlinescoutmanager.co.uk';

        $url = NeoWeb_Connector_OSM_End_Points::getProgramData;
        $formattedURL = (new NeoWeb_Connector_OSM_End_Points)->formatEndPoint($url, $sectionID, $currentTermID, $eveningID);

        $transientID = 'programData_' . $sectionID;

        $programData = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->osmAPICaller($transientID, $formattedURL, 48, array());

        $innerHTML = "";
        $showFooter = false;
        foreach ($programData['badgelinks'] as $key=>$linkedBadges) {
            if (count($linkedBadges) > 0 ) {
                foreach ($linkedBadges as $linkedBadge) {
                    $badgeID = $linkedBadge['badge_id'] . "_0";
                    $badgeLabel = $linkedBadge['label'];
                    $pictureUrl = $linkedBadge['picture'];
                    ob_start();
                    include( get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-print_badge_card.php' );
                    $innerHTML .= ob_get_clean();
                }
                $showFooter = true;
            } else {
                $showFooter = false;
            }
        }

        if ($showFooter) {
            $html = '<div class="card-footer"><div class="row">';
            $html .= $innerHTML;
            $html .= '</div></div>';
        } else {
            $html="";
        }

        echo $html;

    }

    private function roundToTheNearestAnything($value, $roundTo): int
    {
        $mod = $value%$roundTo;
        return $value+($mod<($roundTo/2)?-$mod:$roundTo-$mod);
    }

    public function fetch_badges_by_person ($scoutID, $badgeRecords, $badgetype, $showProgress, $showAwarded) {

        foreach ( $badgeRecords as $badge ) {

            $badgeLabel = $badge['badge'];
            $badgeID = $badge['badge_shortname'];
            $percentage = (float)$badge['status'] * 100;
            $pictureUrl = $badge['picture'];

            if ($percentage == 100) {
                $percentageComplete = 100;
            } else if ($percentage > 0 && $percentage < 100){
                $percentageComplete =  100 - self::roundToTheNearestAnything($percentage, 10);
                $pictureUrl = substr($pictureUrl, 0, -4) . "_" . $percentageComplete . ".png";
            } else {
                $percentageComplete = 0;
            }

            $level = "";
            if ($badgetype == 'challenge') {
                if ( $badge['badge_group'] == 1 ) {
                    if ($showProgress == "true" && $showAwarded == "false") {
                        if ( $badge['awarded'] == 0 ) {
                            include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-print-badge.php');
                        }
                    } else if ($showProgress == "false" && $showAwarded == "true") {
                        if ( $badge['awarded'] != 0 ) {
                            include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-print-badge.php');
                        }
                    } else {
                        include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-print-badge.php');
                    }
                }
            } else if ($badgetype == 'activity') {
                if ( $badge['badge_group'] == 2 ) {
                    if ($showProgress == "true" && $showAwarded == "false") {
                        if ( $badge['awarded'] == 0 ) {
                            include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-print-badge.php');
                        }
                    } else if ($showProgress == "false" && $showAwarded == "true") {
                        if ( $badge['awarded'] != 0 ) {
                            include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-print-badge.php');
                        }
                    } else {
                        include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-print-badge.php');
                    }
                }
            } else if ($badgetype == 'staged') {
                if ( $badge['badge_group'] == 3 ) {
                    $level = "(Level " . $badge['completed'] . ")";
                    if ($showProgress == "true" && $showAwarded == "false") {
                        if ( $badge['awarded'] == 0 ) {
                            include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-print-badge.php');
                        }
                    } else if ($showProgress == "false" && $showAwarded == "true") {
                        if ( $badge['awarded'] != 0 ) {
                            include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-print-badge.php');
                        }
                    } else {
                        include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-print-badge.php');
                    }
                }
            } else {
                if ( $badge['badge_group'] == 3 ) {
                    $level = "(Level " . $badge['completed'] . ")";
                }
                if ($showProgress == "true" && $showAwarded == "false") {
                    if ( $badge['awarded'] == 0 ) {
                        include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-print-badge.php');
                    }
                } else if ($showProgress == "false" && $showAwarded == "true") {
                    if ( $badge['awarded'] != 0 ) {
                        include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-print-badge.php');
                    }
                } else {
                    include(get_option('neoweb-connector-group-manager-path') . 'public/partials/nc-group-manager-public-print-badge.php');
                }
            }
        }
    }

}