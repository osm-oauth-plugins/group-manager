<?php


use utils\NeoWeb_Connector_Auth_Caller;

class Nc_Group_Manager_Register_Plugin_Settings_Page_Programme
{
    private string $pageID;
    private array $plugin_data;

    /**
     * @param $key
     *
     * @return string
     */
    public function get_plugin_data($key): string {
        return $this->plugin_data[$key];
    }

    /**
     * @return array
     */
    public function getAllPluginData(): array {
        return $this->plugin_data;
    }

    /**
     * __constructor.
     */
    public function __construct()
    {
        $this->plugin_data = get_option('neoweb-connector-group-manager');
        $this->pageID = $this->get_plugin_data('pluginSlug') . '-programme-page';
    }

    public function registerFields() {

        if( function_exists('acf_add_local_field_group') ):

            acf_add_local_field_group(array(
                'key' => 'group_' . 'program_options',
                'title' => 'Program Options',
                'fields' => array(
                    array(
                        'key' => 'program_settings_heading',
                        'label' => '',
                        'name' => 'program_settings_heading',
                        'type' => 'message',
                        'message' => "<h1>Program Display Options & Settings</h1>"
                    ),
                    array(
                        'key' => 'field_6018293b204b4564c',
                        'label' => '',
                        'name' => '',
                        'type' => 'message',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => '<p style="text-align: center;" class="neowebNotice">
										For best results, term dates should ideally run back to back in OSM, with the end date of a term set to the day before the start of the next term. Gaps in term dates will result in .
								</p>',
                        'new_lines' => 'wpautop',
                        'esc_html' => 0,
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => $this->pageID,
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'seamless',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
            ));

            acf_add_local_field_group(array(
                'key' => 'group_' . 'program_option_fields',
                'title' => 'Program Options',
                'fields' => array(
                    array(
                        'key' => 'date_format',
                        'label' => 'Date display format',
                        'name' => 'date_format',
                        'type' => 'select',
                        'instructions' => '',
                        'required' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'choices' => array(
                            'd-m-Y' => 'DD-MM-YYYY',
                            'd-m-y' => 'DD-MM-YY',
                            'd/m/Y' => 'DD/MM/YYYY',
                            'd/m/y' => 'DD/MM/YY',
                            'd M Y' => 'DD Month YYYY',
                            'd M y' => 'DD Month YY',
                            'd M' => 'DD Month',
                            'D - d M' => 'DayOfWeek - DD Month',
                            'D - d M YY' => 'DayOfWeek - DD Month YY'
                        ),
                        'default_value' => 'd-m-Y',
                        'allow_null' => 0,
                        'multiple' => 0,
                        'ui' => 0,
                        'return_format' => 'value',
                        'ajax' => 0,
                        'placeholder' => '',
                    ),
                    array(
                        'key' => 'show_future_dates_only',
                        'label' => 'Show future dates only?',
                        'name' => 'show_future_dates_only',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => 'Exclude meetings that have already happened',
                        'default_value' => 0,
                        'ui' => 0,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                    array(
                        'key' => 'show_dates_only',
                        'label' => 'Show dates only? (Hide time values)',
                        'name' => 'show_dates_only',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => 'Exclude meetings times',
                        'default_value' => 0,
                        'ui' => 0,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                    array(
                        'key' => 'show_number_of_parent_helpers_required',
                        'label' => 'Show number of parent helpers required?',
                        'name' => 'show_number_of_parent_helpers_required',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => 'Show the number of parent helpers required for the evening',
                        'default_value' => 0,
                        'ui' => 0,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                    array(
                        'key' => 'show_notes_for_parent_helpers',
                        'label' => 'Show notes for parent helpers?',
                        'name' => 'show_notes_for_parent_helpers',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => 'Show any notes intended for parent helpers',
                        'default_value' => 0,
                        'ui' => 0,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                    array(
                        'key' => 'show_notes_for_parents',
                        'label' => 'Show notes for parents?',
                        'name' => 'show_notes_for_parents',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => 'Show notes intended for parents',
                        'default_value' => 0,
                        'ui' => 0,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                    array(
                        'key' => 'show_game_notes',
                        'label' => 'Show game notes?',
                        'name' => 'show_game_notes',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => 'Show any game/activity notes',
                        'default_value' => 0,
                        'ui' => 0,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                    array(
                        'key' => 'show_pre_meeting_notes',
                        'label' => 'Show pre-meeting notes?',
                        'name' => 'show_pre_meeting_notes',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => 'Show any Pre/Preparation meeting notes',
                        'default_value' => 0,
                        'ui' => 0,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                    array(
                        'key' => 'show_post_meeting_notes',
                        'label' => 'Show post-meeting notes?',
                        'name' => 'show_post_meeting_notes',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => 'Show any post meeting notes',
                        'default_value' => 0,
                        'ui' => 0,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                    array(
                        'key' => 'show_leader_notes',
                        'label' => 'Show leader notes?',
                        'name' => 'show_leader_notes',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => 'Show any notes intended for leaders',
                        'default_value' => 0,
                        'ui' => 0,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                    array(
                        'key' => 'show_linked_badges',
                        'label' => 'Show linked badges?',
                        'name' => 'show_linked_badges',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => 'Show a list of all badges linked to an evening',
                        'default_value' => 0,
                        'ui' => 0,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => $this->pageID,
                        ),
                    ),
                ),
                'menu_order' => 10,
                'position' => 'normal',
                'style' => 'seamless',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'description' => ''
            ));

        endif;

    }

    public function registerShortCodes ($sectionName, $sectionID) {
        if ( function_exists( 'acf_add_local_field_group' ) ):

            $groupName = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->getSectionGroupName($sectionID);

            acf_add_local_field_group(array(
                'key' => 'group_' . 'group_programme' . $sectionID,
                'title' => 'Available [Short-Codes] for ' . $groupName . ' - '. $sectionName,
                'fields' => array(
                    array(
                        'key' => 'programme_short_codes' . $sectionID,
                        'label' => 'Available shortcodes for this section',
                        'name' => 'programme_short_codes' . $sectionID,
                        'type' => 'message',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => '',
                        'new_lines' => 'wpautop',
                        'esc_html' => 0,
                    ),
                    array(
                        'key' => 'programme_short_code' . $sectionID,
                        "label" => "Programme",
                        'name' => 'programme_short_code' . $sectionID,
                        'type' => 'text',
                        'wrapper' => array(
                            'class' => 'shortCodeCopy',
                        ),
                        'readonly'=> 1,
                        'default_value' => '[OSM_Program sectionid="' . $sectionID .'"]',
                    ),
                    array(
                        'key' => 'programme_summery_short_code' . $sectionID,
                        "label" => "Programme Summary Widget",
                        'name' => 'programme_summery_short_code' . $sectionID,
                        'type' => 'text',
                        'wrapper' => array(
                            'class' => 'shortCodeCopy',
                        ),
                        'readonly'=> 1,
                        'default_value' => '[OSM_Program_Summary sectionid="' . $sectionID .'"]',
                    )
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => $this->pageID,
                        ),
                    ),
                ),
                'menu_order' => 30,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'field',
                'hide_on_screen' => '',
                'active' => 1,
                'description' => '',
            ));
        endif;
    }
}