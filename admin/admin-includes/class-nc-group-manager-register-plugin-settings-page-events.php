<?php


use utils\NeoWeb_Connector_Auth_Caller;

class Nc_Group_Manager_Register_Plugin_Settings_Page_Events
{
    private string $pageID;
    private array $plugin_data;

    /**
     * @param $key
     *
     * @return string
     */
    public function get_plugin_data($key): string {
        return $this->plugin_data[$key];
    }

    /**
     * @return array
     */
    public function getAllPluginData(): array {
        return $this->plugin_data;
    }


    /**
     * __constructor.
     */
    public function __construct()
    {
        $this->plugin_data = get_option('neoweb-connector-group-manager');
        $this->pageID = $this->get_plugin_data('pluginSlug') . '-event-page';
    }

    public function registerFields() {

        if( function_exists('acf_add_local_field_group') ):

            acf_add_local_field_group(array(
                'key' => 'group_' . 'event_options',
                'title' => 'Event Options',
                'fields' => array(
                    array(
                        'key' => 'field_event_options',
                        'label' => '',
                        'name' => '',
                        'type' => 'message',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => '<p style="text-align: center;" class="neowebNotice">
										For best results, term dates should ideally run back to back in OSM, with the end date of a term set to the day before the start of the next term. Gaps in term dates will result in .
								</p>',
                        'new_lines' => 'wpautop',
                        'esc_html' => 0,
                    ),
                    array(
                        'key' => 'date_format_events',
                        'label' => 'Date display format',
                        'name' => 'date_format_events',
                        'type' => 'select',
                        'instructions' => '',
                        'required' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'choices' => array(
                            'd-m-Y' => 'DD-MM-YYYY',
                            'd-m-y' => 'DD-MM-YY',
                            'd/m/Y' => 'DD/MM/YYYY',
                            'd/m/y' => 'DD/MM/YY',
                            'd M Y' => 'DD Month YYYY',
                            'd M y' => 'DD Month YY',
                            'd M' => 'DD Month',
                            'D - d M' => 'DayOfWeek - DD Month',
                            'D - d M YY' => 'DayOfWeek - DD Month YY'
                        ),
                        'default_value' => 'd-m-Y',
                        'allow_null' => 0,
                        'multiple' => 0,
                        'ui' => 0,
                        'return_format' => 'value',
                        'ajax' => 0,
                        'placeholder' => '',
                    ),
                    array(
                        'key' => 'show_event_dates_only',
                        'label' => 'Show dates only? (Hide time values)',
                        'name' => 'show_event_dates_only',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => 'Exclude meetings times',
                        'default_value' => 0,
                        'ui' => 0,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => $this->pageID,
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'seamless',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
            ));

        endif;

    }

    public function registerShortCodes ($sectionName, $sectionID) {
        if ( function_exists( 'acf_add_local_field_group' ) ):

            $groupName = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->getSectionGroupName($sectionID);

            acf_add_local_field_group(array(
                'key' => 'group_events' . $sectionID,
                'title' => 'Available [Short-Codes] for ' . $groupName . " - " . $sectionName,
                'fields' => array(
                    array(
                        'key' => 'events_short_codes' . $sectionID,
                        'label' => 'Available shortcodes for this section',
                        'name' => 'events_short_codes' . $sectionID,
                        'type' => 'message',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => '',
                        'new_lines' => 'wpautop',
                        'esc_html' => 0,
                    ),
                    array(
                        'key' => 'events_summery_short_code' . $sectionID,
                        "label" => "Event Summary Widget",
                        'name' => 'events_summery_short_code' . $sectionID,
                        'type' => 'text',
                        'wrapper' => array(
                            'class' => 'shortCodeCopy',
                        ),
                        'readonly'=> 1,
                        'default_value' => '[OSM_Event_Summary sectionid="' . $sectionID .'"]',
                    )
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => $this->pageID,
                        ),
                    ),
                ),
                'menu_order' => 30,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'field',
                'hide_on_screen' => '',
                'active' => 1,
                'description' => '',
            ));
        endif;
    }
}