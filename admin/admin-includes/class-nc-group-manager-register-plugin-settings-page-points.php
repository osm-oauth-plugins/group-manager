<?php


use utils\NeoWeb_Connector_Auth_Caller;

class Nc_Group_Manager_Register_Plugin_Settings_Page_Points
{
    private string $pageID;
    private array $plugin_data;

    /**
     * @param $key
     *
     * @return string
     */
    public function get_plugin_data($key): string {
        return $this->plugin_data[$key];
    }

    /**
     * @return array
     */
    public function getAllPluginData(): array {
        return $this->plugin_data;
    }

    /**
     * __constructor.
     */
    public function __construct()
    {
        $this->plugin_data = get_option('neoweb-connector-group-manager');
        $this->pageID = $this->get_plugin_data('pluginSlug') . '-patrol-points-page';
    }

    public function registerFields() {
        if( function_exists('acf_add_local_field_group') ):

            acf_add_local_field_group(array(
                'key' => 'group_60182740a7b36',
                'title' => 'Lodge / Pack / Patrol Point Settings',
                'fields' => array(
                    array(
                        'key' => 'point_settings_heading',
                        'label' => '',
                        'name' => 'point_settings_heading',
                        'type' => 'message',
                        'message' => "<h1>Lodge / Six / Patrol - Point Settings</h1>"
                    ),
                    array(
                        'key' => 'show_points',
                        'label' => 'Show points?',
                        'name' => 'show_points',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => 'Show each lodge/six/patrols points?',
                        'default_value' => 0,
                        'ui' => 0,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                    array(
                        'key' => 'text_to_show_before_points',
                        'label' => 'Text to show before points',
                        'name' => 'text_to_show_before_points',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'show_points',
                                    'operator' => '==',
                                    'value' => '1',
                                ),
                            ),
                        ),
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'text_to_show_after_points',
                        'label' => 'Text to show after points',
                        'name' => 'text_to_show_after_points',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'show_points',
                                    'operator' => '==',
                                    'value' => '1',
                                ),
                            ),
                        ),
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'show_young_people',
                        'label' => 'Show young people?',
                        'name' => 'show_young_people',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => 'Show a list of young people per lodge/six/patrol',
                        'default_value' => 0,
                        'ui' => 0,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                    array(
                        'key' => 'points_young_person_name_display',
                        'label' => 'Young Person Name Display',
                        'name' => 'points_young_person_name_display',
                        'type' => 'select',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'show_young_people',
                                    'operator' => '==',
                                    'value' => '1',
                                )
                            )
                        ),
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'choices' => array(
                            'id_only' => 'OSM ID',
                            'firstName' => 'First name only',
                            'firstName+' => 'First name + First 2 characters from surname',
                            'firstName+1' => 'First name + First character from surname',
                            'lastName' => 'Surname only',
                            'lastname+' => 'Surname + First 2 characters from firstname',
                            'lastname+1' => 'Surname + First characters from firstname',
                            'initial' => 'Initial + Surname',
                            'fullname' => 'Surname + Firstname',
                            'fullname2' => 'Firstname + Surname',
                        ),
                        'default_value' => 'firstName+',
                        'allow_null' => 0,
                        'multiple' => 0,
                        'ui' => 0,
                        'return_format' => 'value',
                        'ajax' => 0,
                        'placeholder' => '',
                    ),
                    array(
                        'key' => 'show_young_persons_rank',
                        'label' => 'Show young persons rank?',
                        'name' => 'show_young_persons_rank',
                        'type' => 'true_false',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => 'Show the young persons rank if set, for example - Lodge Leader',
                        'default_value' => 0,
                        'ui' => 0,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => $this->pageID,
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'seamless',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
            ));

        endif;
    }

    public function registerShortCodes ($sectionName, $sectionID) {
        if ( function_exists( 'acf_add_local_field_group' ) ):

            $groupName = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->getSectionGroupName($sectionID);

            acf_add_local_field_group(array(
                'key' => 'group_patrol' . $sectionID,
                'title' => 'Available [Short-Codes] for ' . $groupName . ' - ' . $sectionName,
                'fields' => array(
                    array(
                        'key' => 'patrol_short_codes' . $sectionID,
                        'label' => 'Available shortcodes for this section',
                        'name' => 'patrol_short_codes' . $sectionID,
                        'type' => 'message',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => '',
                        'new_lines' => 'wpautop',
                        'esc_html' => 0,
                    ),
                    array(
                        'key' => 'section_patrol_points_short_code' . $sectionID,
                        "label" => "Section Patrol Points",
                        'name' => 'section_patrol_points_short_code' . $sectionID,
                        'type' => 'text',
                        'wrapper' => array(
                            'class' => 'shortCodeCopy',
                        ),
                        'readonly'=> 1,
                        'default_value' => '[OSM_Section_Points sectionid="' . $sectionID .'"]',
                    )
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => $this->pageID,
                        ),
                    ),
                ),
                'menu_order' => 30,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'field',
                'hide_on_screen' => '',
                'active' => 1,
                'description' => '',
            ));
        endif;
    }

}