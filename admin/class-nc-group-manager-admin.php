<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://neoweb.co.uk
 * @since      1.0.0
 *
 * @package    Nc_Group_Manager
 * @subpackage Nc_Group_Manager/admin
 */

use utils\NeoWeb_Connector_Admin_Notifications;
use utils\NeoWeb_Connector_Auth_Caller;
use utils\Neoweb_Connector_Helper_Functions;
use utils\NeoWeb_Connector_Licence_Manager;
use utils\Neoweb_Connector_Transient_Manager;

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Nc_Group_Manager
 * @subpackage Nc_Group_Manager/admin
 * @author     Jaco Mare <jaco.mare@neoweb.co.uk>
 */
class Nc_Group_Manager_Admin {


    /**
     * @var array
     */
    private array $plugin_data;

    /**
     *
     * @return array
     */
    private array $productPages;

    /**
     * @var array
     */
    private array $genericProductPages;

    /**
     * @var array
     */
    private array $productAPIPages;

    /**
     * @param $key
     *
     * @return string
     */
    public function get_plugin_data($key): string {
        return $this->plugin_data[$key];
    }

    /**
     *
     * @return array
     */
    public function getAllPluginData(): array {
        return $this->plugin_data;
    }

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     */
    public function __construct() {
        $this->plugin_data = get_option('neoweb-connector-group-manager');
        $this->genericProductPages = array(
            "licences" => array(
                "protected" => false,
                "page_title" => "Product Licence(s)",
                "menu_title" => "Product Licence(s)",
                "button_label" => "Save personal details",
                "notification_label" => "Personal details updated"
            ),
            "support" => array(
                "protected" => false,
                "page_title" => "NeoWeb Connector - Support Settings",
                "menu_title" => "Support Settings",
                "button_label" => "Update",
                "notification_label" => "Details updated"
            ),
            "cache" => array(
                "protected" => false,
                "page_title" => "NeoWeb Connector - Cache Settings",
                "menu_title" => "Cache Settings",
                "button_label" => "Update",
                "notification_label" => "Details updated"
            )
        );
        $this->productPages = array(
            "app-settings" => array(
                "protected" => true,
                "page_title" => "OSM Application Settings",
                "menu_title" => "Application Settings",
                "button_label" => "Authenticate plugin",
                "notification_label" => "Application details updated"
            ),
            "debug" => array(
                "protected" => true,
                "page_title" => "Debug Settings",
                "menu_title" => "Debug Settings",
                "button_label" => "Update",
                "notification_label" => "Details updated"
            ),
            "patrol-points-page" => array(
                "protected" => true,
                "page_title" => "Section Points Settings",
                "menu_title" => "Points Settings",
                "button_label" => "Update",
                "notification_label" => "Details updated"
            ),
            "programme-page" => array(
                "protected" => true,
                "page_title" => "Section Programme Settings",
                "menu_title" => "Programme Settings",
                "button_label" => "Update",
                "notification_label" => "Details updated"
            ),
            "event-page" => array(
                "protected" => true,
                "page_title" => "Section Events Settings",
                "menu_title" => "Events Settings",
                "button_label" => "Update",
                "notification_label" => "Details updated"
            ),
            "badges-page" => array(
                "protected" => true,
                "page_title" => "Section Badges Settings",
                "menu_title" => "Badges Settings",
                "button_label" => "Update",
                "notification_label" => "Details updated"
            )
        );
        $this->productAPIPages = array(
            "resources" => array(
                "protected" => true,
                "page_title" => "Available OSM Resources",
                "menu_title" => "Available Resources",
                "button_label" => "Update",
                "notification_label" => "Details updated"
            )
        );
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Nc_Events_Manager_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Nc_Events_Manager_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style( $this->get_plugin_data('pluginName'), plugin_dir_url( __FILE__ ) . 'css/nc-group-manager-admin.css', array(), $this->get_plugin_data('pluginVersion'), 'all' );

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Nc_Events_Manager_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Nc_Events_Manager_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script( $this->get_plugin_data('pluginName'), plugin_dir_url( __FILE__ ) . 'js/nc-group-manager-admin.js', array( 'jquery' ), $this->get_plugin_data('pluginVersion'), false );

    }

    /**
     * Load all ACF Option pages for the plugin
     */
    public function load_admin_pages() {
        (new NeoWeb_Connector_Register_Product_Pages('neoweb-connector-group-manager'))->registerFieldsFromJsonConfig($this->genericProductPages);

        $currentStatus = get_field($this->get_plugin_data('pluginSlug') . '_licence_status', 'option');
        if ($currentStatus == 'Active') {
            (new NeoWeb_Connector_Register_Product_Pages('neoweb-connector-group-manager'))->registerPluginPages($this->productPages);
            (new NeoWeb_Connector_Register_Product_Pages('neoweb-connector-group-manager'))->registerPluginPages($this->productAPIPages);
            (new NeoWeb_Connector_Register_Product_Pages('neoweb-connector-group-manager'))->registerFieldsFromJsonConfig($this->productPages);
            $this->load_admin_page_logos();
        }
    }

    /**
     * Load all ACF Option pages for the plugin
     */
    public function loadAdminPageFields() {
        $this->load_admin_page_fields();
    }

    /**
     * Load all ACF Option page logos
     */
    public function load_admin_page_logos() {
        $locationArray = array();
        //Create logo field for default plugin pages
        foreach ($this->productPages as $pageSlug => $pageTitle) {
            $productPage = array(
                array(
                    'param'    => 'options_page',
                    'operator' => '==',
                    'value'    => $this->get_plugin_data('pluginSlug') . "-" . $pageSlug,
                ),
            );
            array_push($locationArray, $productPage);
        }

        //Create logo field for resource pages
        foreach ($this->productAPIPages as $pageSlug => $pageTitle) {
            $productPage = array(
                array(
                    'param'    => 'options_page',
                    'operator' => '==',
                    'value'    => $this->get_plugin_data('pluginSlug') . "-" . $pageSlug,
                ),
            );
            array_push($locationArray, $productPage);
        }

        (new NeoWeb_Connector_Register_Product_Pages('neoweb-connector-group-manager'))->registerProductPageLogos($locationArray, "group_manager");
    }

    /**
     * Load all ACF Option page fields
     */
    public function load_admin_page_fields() {
        /**
         * Check we have app details saved and that we have an access token from a previous authentication call,
         * then try to renew the accessToken if needed.
         **/
        if (get_field($this->get_plugin_data('pluginSlug') . "_osm_oauth_client_id", "option") &&
            get_field($this->get_plugin_data('pluginSlug') . "_osm_oauth_secret", "option")) {

            $accessToken = get_option($this->get_plugin_data('pluginSlug') . "_osm_access_token_data");
            $accessTokenExpiry = get_option($this->get_plugin_data('pluginSlug') . "_osm_access_token_expiry");

            if ($accessToken != null) {
                //We have an access token
                if ((new Neoweb_Connector_Helper_Functions)->hasExpired($accessTokenExpiry)) {
                    //if the access token has expired get a new one before continuing
                    (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->osm_authenticate();
                    //get the refreshed token
                    $accessToken = get_option($this->get_plugin_data('pluginSlug') . "_osm_access_token_data");
                }

                //Load available resources
                $authCaller = (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()));
                $sections = $authCaller->getAvailableSectionsByGroup();
                $sectionsMetaData = $authCaller->getTransientMetaData('resourceData');
                (new NeoWeb_Connector_Register_Resources_Pages('neoweb-connector-group-manager'))->prepareResourcesFields($sections, $sectionsMetaData);

                //Register Patrol Points Page
                $patrolPointsPage = new Nc_Group_Manager_Register_Plugin_Settings_Page_Points();
                $patrolPointsPage->registerFields();

                //Register Program Page
                $programmePage = new Nc_Group_Manager_Register_Plugin_Settings_Page_Programme();
                $programmePage->registerFields();

                //Register Events Page
                $eventPage = new Nc_Group_Manager_Register_Plugin_Settings_Page_Events();
                $eventPage->registerFields();

                //Register Badge Page
                $badgeProgressPage = new Nc_Group_Manager_Register_Plugin_Settings_Page_Badges();
                $badgeProgressPage->registerFields();

                //Register ShortCodes for all pages
                foreach ($sections as $groupID => $groupSections) {
                    foreach ($groupSections as $groupSection) {
                        $badgeProgressPage->registerShortCodes($groupSection['section_name'], $groupSection['section_id'], $groupSection['section_type']);
                        $eventPage->registerShortCodes($groupSection['section_name'], $groupSection['section_id']);
                        $programmePage->registerShortCodes($groupSection['section_name'], $groupSection['section_id']);
                        $patrolPointsPage->registerShortCodes($groupSection['section_name'], $groupSection['section_id']);
                    }
                }
            }
        }
    }

    public function trigger_publish_click_scripts() {
        $screen = get_current_screen();
        if ( $screen->id == "toplevel_page_neoweb-connector-group-manager-app-settings" ) {

            $osm_oauth_client_id = get_field($this->get_plugin_data('pluginSlug') . "_osm_oauth_client_id", "option");
            $osm_oauth_secret = get_field($this->get_plugin_data('pluginSlug') . "_osm_oauth_secret", "option");

            if ($osm_oauth_client_id && $osm_oauth_secret) {
                (new NeoWeb_Connector_Auth_Caller($this->getAllPluginData()))->osm_authenticate();
                $accessToken = get_option($this->get_plugin_data('pluginSlug') . "_accessToken");
                if (isset($accessToken)) {
                    (new NeoWeb_Connector_Admin_Notifications())->add_flash_notice("Authentication with OSM application completed successfully", "success", false);
                    //Clear the transient for this product just encase we had some rouge transients
                    $results = (new Neoweb_Connector_Transient_Manager($this->get_plugin_data('pluginSlug')))->wds_delete_transients();
                }
            } else {
                (new NeoWeb_Connector_Admin_Notifications())->add_flash_notice("The Client ID and Secret has not been entered, 
			    authorisation to OSM has been aborted. Please complete the instruction on this page before trying again.", "error", false);
            }
        }
    }
}
