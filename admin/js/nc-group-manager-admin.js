(function( $ ) {
	'use strict';

	$(function() {

		const productSlug = "neoweb_connector_group_manager"

		$('#'+ productSlug +'_trigger_APITest').on('click', function() {
			$(this).prop('disabled', true);
			var data = {
				'action': 'trigger_api_test_' + productSlug
			};

			$.post(ajaxurl, data, function(data) {
				console.log(data);
				alert(data['status'] + " : " + data['message']);
				location.reload();
			});
		});

		$('#'+ productSlug +'_trigger_LogRefresh').on('click', function() {
			$(this).prop('disabled', true);
			var data = {
				'action': 'trigger_log_refresh_' + productSlug
			};

			$.post(ajaxurl, data, function() {
				location.reload();
			});
		});

		$('#'+ productSlug +'_trigger_TransientLogRefresh').on('click', function() {
			$(this).prop('disabled', true);
			var data = {
				'action': 'trigger_transient_log_refresh_' + productSlug
			};

			$.post(ajaxurl, data, function() {
				location.reload();
			});
		});

		$('#'+ productSlug +'_trigger_CacheRefresh').on('click', function() {
			$(this).prop('disabled', true);
			var data = {
				'action': 'trigger_cache_refresh_' + productSlug
			};

			$.post(ajaxurl, data, function() {
				location.reload();
			});
		});

		$('#'+ productSlug +'_register').on('click', function() {
			$(this).prop('disabled', true);
			const data = {
				'action': 'trigger_create_licence_key_request_' + productSlug
			};

			$.post(ajaxurl, data, function() {
				//location.reload();
				const data = {
					'action': 'trigger_activate_licence_key_request_' + productSlug
				};

				$.post(ajaxurl, data, function() {
					location.reload();
				});
			});
		});

		$('#'+ productSlug +'_check').on('click', function() {
			$(this).prop('disabled', true);
			const data = {
				'action': 'trigger_check_licence_key_request_' + productSlug
			};

			$.post(ajaxurl, data, function(response) {
				if (response) {
					alert("Licence check passed");
				} else {
					alert("Licence check failed, the licence will be reset, please request a new licence.")
				}
				location.reload();
			});
		});

		$('div.disabled').each(function () {
			$(this).find('input:radio:not(:checked)').prop('disabled', true);
		});

	});

})( jQuery );
