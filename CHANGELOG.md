# Change Log
##Version 1.3 (17/08/2021)
* Minor bug fixes, around licence timing out
##Version 1.2.1 (22/05/2021)
* Update api handler
* Remove need to clear licence after upgrade/error

##Version 1.2 (26/04/2021)
* Update cache manager
* Update powered by text
* Improve accordion
* Add required product version details

##Version 1.1.2 (22/03/2021)
* Add additional name display options
* Fix bug affecting name display
* Fix mislabelled event summary transient, causing event summary not to work

##Version 1.1.1
* Add date formatting options for date displays
* Add option to omit times from Program and event summaries

##Version 1.1
* Fix app instructions
* Improve admin page load times
* Improve widgets to include start and end times  
* Other minor bug fixes

##Version 1.0
Replacement for NeoWeb Authenticator Plugin adding oAuth support and splitting into separate plugins/add-ons